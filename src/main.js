import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 解决移动端300ms点击延迟问题
import fastClick from 'fastclick'
fastClick.attach(document.body)
    // 清除默认样式
import '@/assets/css/reset.scss';
import 'swiper/css/swiper.css'
// 引入图标
import '@/assets/icon/iconfont.css'

Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')