import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'home',
        component: () =>
            import ( /* webpackChunkName: "home" */ '../views/home.vue')
    },
    {
        path: '/money',
        name: 'money',
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/money.vue')
    },
    {
        path: '/ious',
        name: 'ious',
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/ious.vue')
    }, {
        path: '/raise',
        name: 'raise',
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/raise.vue')
    }, {
        path: '/about',
        name: 'about',
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/about.vue')
    },
]

const router = new VueRouter({
    routes,
    // 解决当页面滑动到下方时点击进入详情页不在顶部问题
    scrollBehavior: function(to, from, savedPosition) {
        return savedPosition || { x: 0, y: 0 }
    }
})

export default router